# Neuste Änderungen (Stand 06.05.2021)

## Dedizierter Channel für Termine

**A.W.E.S.O.M-O 4000** sendet deine Termine jetzt in den Channel #termine

## ACHTUNG: BREAKING CHANGE

Auf den von @MightyTrash mehrfach geäußerten Wunsch wurde das Prefix von `$` auf `!` geändert

